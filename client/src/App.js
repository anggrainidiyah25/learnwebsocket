import "./App.css";
import io from "socket.io-client";
import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Input from "@mui/material/Input";
import Typography from "@mui/material/Typography";

const socket = io.connect("http://localhost:3001");

function App() {
  const [message, setMessage] = useState("");
  const [messageReceived, setMessageReceived] = useState("");
  const [room, setRoom] = useState("");

  const joinRoom = () => {
    if (room !== "") {
      socket.emit("join_room", room);
    }
  };

  const sendMessage = () => {
    socket.emit("send_message", { message, room });
  };

  useEffect(() => {
    socket.on("receive_message", (data) => {
      setMessageReceived(data.message);
    });
  }, [socket]);

  return (
    <div className="App">
      <Typography sx={{ margin: "10px", fontSize: "36px", textAlign: "center", fontWeight: "bold" }}>Meeting Room</Typography>
      <div className="kotak">
        <Box component="form" sx={{ backgroundColor: "#ccac93", height: "200px", width: "500px", borderRadius: "4px" }} noValidate autoComplete="off">
          <Typography sx={{ margin: "10px" }}>Room Number</Typography>
          <Input
            sx={{ height: "94px", padding: "20px", width: "420px", fontSize: "48px" }}
            onChange={(event) => {
              setRoom(event.target.value);
            }}
          />
          <Button onClick={joinRoom} variant="outline" sx={{ marginTop: "8px", marginLeft: "299px", border: "2px", borderStyle: "solid", borderColor: "black" }}>
            Join Room
          </Button>
        </Box>

        <Box sx={{ backgroundColor: "#b8b8b8", height: "200px", width: "500px", borderRadius: "4px", marginLeft: "16px" }}>
          <Typography sx={{ margin: "10px" }}>Message</Typography>

          <Input
            sx={{ height: "94px", padding: "20px", width: "420px", fontSize: "48px" }}
            onChange={(event) => {
              setMessage(event.target.value);
            }}
          />
          <Button onClick={sendMessage} variant="outline" sx={{ marginTop: "8px", marginLeft: "272px", border: "2px", borderStyle: "solid", borderColor: "black" }}>
            Send Message
          </Button>
        </Box>
      </div>
      <Box sx={{ borderRadius: "4px", boxShadow: "300px", backgroundColor: "#b8b8b8", width: "63.6rem", marginLeft: "166px" }}>
        <div className="pesan">
          <h1> Message:</h1>
          <h2>{messageReceived}</h2>
        </div>
      </Box>
    </div>
  );
}

export default App;
